//
//  SubclassConfigViewController.m
//  LogInAndSignUpDemo
//
//  Created by Yuchen Wen on 4/1/14.
//  Copyright 2014 Yuchen Wen All rights reserved.
//

#import "SubclassConfigViewController.h"
#import "MyLogInViewController.h"
#import "MySignUpViewController.h"

@implementation SubclassConfigViewController


#pragma mark - UIViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([PFUser currentUser]) {
        self.welcomeLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Welcome %@!", nil), [[PFUser currentUser] username]];
    } else {
        self.welcomeLabel.text = NSLocalizedString(@"Not logged in", nil);
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    NSLog(@"%@",[PFUser currentUser]);
    // Check if user is logged in
    if (![PFUser currentUser]) {        
        // Customize the Log In View Controller
        MyLogInViewController *logInViewController = [[MyLogInViewController alloc] init];
        logInViewController.delegate = self;
      //  logInViewController.facebookPermissions = @[@"friends_about_me"];
        logInViewController.fields = PFLogInFieldsLogInButton|PFLogInFieldsPasswordForgotten| PFLogInFieldsUsernameAndPassword| PFLogInFieldsSignUpButton;
        
        // Customize the Sign Up View Controller
        MySignUpViewController *signUpViewController = [[MySignUpViewController alloc] init];
        signUpViewController.delegate = self;
        signUpViewController.fields = PFSignUpFieldsDefault | PFSignUpFieldsAdditional;
        logInViewController.signUpController = signUpViewController;
        
        // Present Log In View Controller
        [self presentViewController:logInViewController animated:YES completion:NULL];
    }
}


#pragma mark - PFLogInViewControllerDelegate

// Sent to the delegate to determine whether the log in request should be submitted to the server.
- (BOOL)logInViewController:(PFLogInViewController *)logInController shouldBeginLogInWithUsername:(NSString *)username password:(NSString *)password {
    if (username && password && username.length && password.length) {
        return YES;
    }
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    return NO;
}

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];

  if (![[user objectForKey:@"emailVerified"] boolValue]) {
    // Refresh to make sure the user did not recently verify
    [user refresh];
   
    if (![[user objectForKey:@"emailVerified"] boolValue]) {
      [PFUser logOut];
      
      UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Verification"
                                                      message:@"Please verify your email before login."
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
      [alert show];
       NSLog(@"last!!!!!!!!!!!!");
    //  SubclassConfigViewController *plainColorVC = [[SubclassConfigViewController alloc] init];
     // [self.drawer replaceCenterViewControllerWithViewControllerr:plainColorVC];
      return;
    }
  }
  NSArray *colors = @[[UIColor colorWithRed:237.0f/255.0f green:195.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:147.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:9.0f/255.0f blue:0.0f/255.0f alpha:1.0f]
                      ];
  ProfileController *profile = [[ProfileController alloc] initWithLoginToProfile:1];
  profile.view.backgroundColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
  [self.drawer replaceCenterViewControllerWithViewControllerr:profile];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Fail to login", nil) message:NSLocalizedString(@"Wrong Username or Password. Please try again.", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    NSLog(@"User dismissed the logInViewController");
}


#pragma mark - PFSignUpViewControllerDelegate

// Sent to the delegate to determine whether the sign up request should be submitted to the server.
- (BOOL)signUpViewController:(PFSignUpViewController *)signUpController shouldBeginSignUp:(NSDictionary *)info {
    BOOL informationComplete = YES;
    BOOL emailcheck = YES;
    for (id key in info) {
        NSString *field = [info objectForKey:key];
      if (key != @"email") {
        NSLog(@"key is not email");
      } else {
        if ([field rangeOfString:@"umich.edu" options:NSCaseInsensitiveSearch].location == NSNotFound) {
          emailcheck = NO;
        } else {
          NSLog(@"string contains umich.edu");
        }
      }
        if (!field || field.length == 0) {
            informationComplete = NO;
            break;
        }                  
    }
    
    if (!informationComplete) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Missing Information", nil) message:NSLocalizedString(@"Make sure you fill out all of the information!", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
    }
  if (!emailcheck) {
    informationComplete = NO;
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Not Umich email", nil) message:NSLocalizedString(@"Please sign up with your umich email (@umich.edu).", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil] show];
  }
  
    return informationComplete;
}

// Sent to the delegate when a PFUser is signed up.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    [self dismissViewControllerAnimated:YES completion:NULL];
 
  [PFUser logOut];
  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Verification"
                                                  message:@"Please verify your email before login."
                                                 delegate:nil
                                        cancelButtonTitle:@"OK"
                                        otherButtonTitles:nil];
  [alert show];
/*
 NSArray *colors = @[[UIColor colorWithRed:237.0f/255.0f green:195.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:147.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:9.0f/255.0f blue:0.0f/255.0f alpha:1.0f]
                      ];
  ProfileController *profile = [[ProfileController alloc] initWithLoginToProfile:1];
  profile.view.backgroundColor = colors[0];
  [self.drawer replaceCenterViewControllerWithViewControllerr:profile];
 */
  
}

// Sent to the delegate when the sign up attempt fails.
- (void)signUpViewController:(PFSignUpViewController *)signUpController didFailToSignUpWithError:(NSError *)error {
    NSLog(@"Failed to sign up...");
}

// Sent to the delegate when the sign up screen is dismissed.
- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
    NSLog(@"User dismissed the signUpViewController");
}


#pragma mark - ()

- (IBAction)logOutButtonTapAction:(id)sender {
    [PFUser logOut];
  MyLogInViewController *logInViewController = [[MyLogInViewController alloc] init];
  logInViewController.delegate = self;
  //  logInViewController.facebookPermissions = @[@"friends_about_me"];
  //   logInViewController.fields = PFLogInFieldsUsernameAndPassword | PFLogInFieldsTwitter | PFLogInFieldsFacebook | PFLogInFieldsSignUpButton | PFLogInFieldsDismissButton;
  
  // Customize the Sign Up View Controller
  MySignUpViewController *signUpViewController = [[MySignUpViewController alloc] init];
  signUpViewController.delegate = self;
  signUpViewController.fields = PFSignUpFieldsDefault | PFSignUpFieldsAdditional;
  logInViewController.signUpController = signUpViewController;
  
  // Present Log In View Controller
  [self presentViewController:logInViewController animated:YES completion:NULL];
}

@end
