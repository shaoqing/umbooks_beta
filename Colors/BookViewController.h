//
//  BookViewController.h
//  Colors
//
//  Created by Guan Wang on 3/19/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "CourseViewController.h"
#import "ICSDrawerController.h"
#import "PosterTableViewController.h"
#import <MessageUI/MessageUI.h>

@interface BookViewController : UIViewController<MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) CourseViewController* frontView;
@property (strong, nonatomic) PosterTableViewController *backView;
@property MFMailComposeViewController* mc;
@property (nonatomic) BOOL goingToFrontView;
@property(nonatomic, weak) ICSDrawerController *drawer;

@property (strong, nonatomic) UIButton * FlipButton;

@end
