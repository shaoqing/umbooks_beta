//
//  BookAddingViewController.m
//  Colors
//
//  Created by Shaoqing Zhu on 3/27/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "BookAddingViewController.h"
#import <QuartzCore/QuartzCore.h>
@interface BookAddingViewController ()

@end

@implementation BookAddingViewController

@synthesize book,bookinfo,bookname,author,isEdit,myid,objid;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
- (id)initWithCourseID:(NSNumber *)courseid; {
    book = [[Book alloc]init];
    book.courseid = courseid;
    
    return self;
}
-(void) AlertShow {
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self addbookView]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK", @"Cancel", nil]];
    [alertView setDelegate:self];
    
    NSLog(@"Alert View Ready");
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
        if (buttonIndex == 0) {
            UIView* content = alertView.containerView;
            UITextField* name = [content viewWithTag:1];
            UITextField* author = [content viewWithTag:2];
            //UITextField* isbn = [content viewWithTag:3];
            UITextView* info = [content viewWithTag:4];
            
            //book.isbn = isbn.text;
            book.author = author.text;
            book.bookinfo = info.text;
            book.title = name.text;
            if (isEdit) {
                [self updateToParce:book];
                [[NSNotificationCenter defaultCenter] postNotification:[NSNotification                                                                notificationWithName:@"Book Edited"                                                                object:book]];
            }
            else {
                [self saveToParce:book];
            }
        }
        
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
    [alertView close];
}
-(void) saveToParce: (Book*) bk {
    NSLog(@"AdP");
    // Create the Courses Table
    PFObject* bookObj = [PFObject objectWithClassName:@"Books"];
    NSLog(@"AdB");
    
    PFQuery *qry = [[PFQuery alloc]initWithClassName:@"IDs"];
    [qry getObjectInBackgroundWithId:@"Ax9vTXuwEG" block:^(PFObject *object, NSError *error) {
        NSNumber* currentid=[object objectForKey:@"currentID"];
        [bookObj setObject:currentid forKey:@"bookID"];
        //[bookObj setObject:bk.isbn forKey:@"ISBN"];
        [bookObj setObject:bk.bookinfo forKey:@"bookInfo"];
        [bookObj setObject:bk.author forKey:@"Author"];
        [bookObj setObject:bk.title forKey:@"Title"];
        [bookObj setObject:bk.courseid forKey:@"courseID"];
        [bookObj saveInBackground];
    }];
    
    PFObject *idobj = [PFObject objectWithoutDataWithClassName:@"IDs" objectId:@"Ax9vTXuwEG"];
    [idobj incrementKey:@"currentID"];
    [idobj saveInBackground];
    
}

-(void) updateToParce: (Book*) bk {
    // Create a pointer to an object of class Point with id dlkj83d
    PFObject *bookObj = [PFObject objectWithoutDataWithClassName:@"Books" objectId:objid];
    //[bookObj setObject:bk.isbn forKey:@"ISBN"];
    [bookObj setObject:bk.bookinfo forKey:@"bookInfo"];
    [bookObj setObject:bk.author forKey:@"Author"];
    [bookObj setObject:bk.title forKey:@"Title"];
    [bookObj saveInBackground];
}

- (UIView *)addbookView
{
    UIView *bookView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 310)];

    
     bookname = [[UITextField alloc]initWithFrame:CGRectMake(10, 30, 270, 30)];
    bookname.tag = 1;
    bookname.delegate = self;
    bookname.placeholder = @"Book Name";
    [bookname setAutocorrectionType:UITextAutocorrectionTypeNo];
    [bookname setBackgroundColor:[UIColor whiteColor]];
    [bookname setReturnKeyType:UIReturnKeyNext];
    [bookname setBorderStyle:UITextBorderStyleRoundedRect];
    [bookname setClearButtonMode:UITextFieldViewModeWhileEditing];

    
     author = [[UITextField alloc]initWithFrame:CGRectMake(10, 80, 270, 30)];
    author.tag = 2;
    author.delegate = self;
    author.placeholder = @"Author";
    [author setAutocorrectionType:UITextAutocorrectionTypeNo];
    [author setBackgroundColor:[UIColor whiteColor]];
    [author setReturnKeyType:UIReturnKeyNext];
    [author setBorderStyle:UITextBorderStyleRoundedRect];
    [author setClearButtonMode:UITextFieldViewModeWhileEditing];

    
     /*isbn = [[UITextField alloc]initWithFrame:CGRectMake(10, 130, 270, 30)];
    isbn.tag = 3;
    isbn.delegate = self;
    isbn.placeholder = @"ISBN";
    [isbn setAutocorrectionType:UITextAutocorrectionTypeNo];
    [isbn setBackgroundColor:[UIColor whiteColor]];
    [isbn setReturnKeyType:UIReturnKeyNext];
    [isbn setBorderStyle:UITextBorderStyleRoundedRect];
    [isbn setClearButtonMode:UITextFieldViewModeWhileEditing];*/

    
     bookinfo = [[GCPlaceholderTextView alloc]initWithFrame:CGRectMake(10, 130, 270, 150)];
    bookinfo.tag = 4;
    bookinfo.delegate = self;
    bookinfo.placeholder = @"Book Description";
    bookinfo.placeholderColor = [UIColor colorWithRed:200/255.0f green:200/255.0f blue:200/255.0f alpha:1.0f];
    bookinfo.font = [UIFont systemFontOfSize:17];
    [bookinfo setAutocorrectionType:UITextAutocorrectionTypeNo];
    [bookinfo setBackgroundColor:[UIColor whiteColor]];
    [bookinfo setReturnKeyType:UIReturnKeyDone];
    //To make the border look very close to a UITextField
    [bookinfo.layer setBorderColor:[[[UIColor grayColor] colorWithAlphaComponent:0.5] CGColor]];
    [bookinfo.layer setBorderWidth:0.5];
    //The rounded corner part, where you specify your view's corner radius:
    bookinfo.layer.cornerRadius = 5;
    bookinfo.clipsToBounds = YES;
    [bookinfo setTextAlignment:UITextAlignmentLeft];
    //[courseinfo setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    if (isEdit) {
        PFQuery* query = [[PFQuery alloc]initWithClassName:@"Books"];
        [query whereKey:@"bookID" equalTo:myid];
        NSLog(@"Editing Book: %@",myid);
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            bookname.text = [object objectForKey:@"Title"];
            bookinfo.text = [object objectForKey:@"bookInfo"];
            author.text = [object objectForKey:@"Author"];
            //isbn.text = [object objectForKey:@"ISBN"];
            objid = object.objectId;
        }];
        
    }
    
    [bookView addSubview:bookname];
    [bookView addSubview:author];
    //[bookView addSubview:isbn];
    [bookView addSubview:bookinfo];
    return bookView;
}

-(BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    BOOL ret = NO;
    if(textField == self.bookname) {
        [self.author becomeFirstResponder];
    } else if (textField == self.author) {
        [self.bookinfo becomeFirstResponder  ];
    } else {
        ret = YES;
        [textField resignFirstResponder];
    }
    return ret;
}
@end
