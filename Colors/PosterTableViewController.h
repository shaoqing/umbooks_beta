//
//  PosterTableViewController.h
//  UMBooks
//
//  Created by Shaoqing Zhu on 3/18/14.
//  Copyright (c) 2014 Shaoqing Zhu. All rights reserved.
//

#import <Parse/Parse.h>
#import "Datatypes.h"
#import "PosterAddingViewController.h"
#import "BookAddingViewController.h"
#import "CourseAddingViewController.h"
#import "FilterViewController.h"
#import "ICSDrawerController.h"
#import "PosterDetailView.h"

@interface PosterTableViewController : PFQueryTableViewController
@property Filter* myfilter;
@property  NSMutableArray* CurrentObj;
@property PosterAddingViewController *posteraddingview;
@property PosterDetailView* posterdetail;
@property FilterViewController* filterview;

@property NSNumber* myid;
@property (strong, nonatomic) UIButton * aButton;
@property (strong, nonatomic) UIButton * daButton;
@property(nonatomic, weak) ICSDrawerController *drawer;

@property UIView* topView;
-(id) initWithBookid:(NSNumber *)bookid withFilter:(Filter*)filter;

@end
