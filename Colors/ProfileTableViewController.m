//
//  PosterTableViewController.m
//  UMBooks
//
//  Created by Shaoqing Zhu on 3/18/14.
//  Copyright (c) 2014 Shaoqing Zhu. All rights reserved.
//

#import "ProfileTableViewController.h"
#import "CHTumblrMenuView.h"
#import "GHContextMenuView.h"

@interface ProfileTableViewController ()

@end

@implementation ProfileTableViewController
@synthesize myusername,myid;

-(id) initWithUsername:(NSString *)username;

{
  self = [super initWithClassName:username];
  
  if (self) {
    // Custom the table
    
    // The className to query on
    self.parseClassName = @"Posters";
    
    // The key of the PFObject to display in the label of the default cell style
    self.textKey = @"note";
    
    // The title for this table in the Navigation Controller.
    self.title = @"Poster";
    
    // Whether the built-in pull-to-refresh is enabled
    self.pullToRefreshEnabled = YES;
    
    // Whether the built-in pagination is enabled
    self.paginationEnabled = YES;
    
    // The number of objects to show per page
    self.objectsPerPage = 10;
  }
  _CurrentObj = [[NSMutableArray alloc] init];
    myusername = username;
  
  return self;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  // Uncomment the following line to preserve selection between presentations.
  // self.clearsSelectionOnViewWillAppear = NO;
  
  // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
   self.navigationItem.rightBarButtonItem = self.editButtonItem;
  
  // Do any additional setup after loading the view.
    CALayer *layer = self.tableView.layer;
    [layer setMasksToBounds:YES];
    [layer setCornerRadius: 6.0];
    [layer setBorderWidth:1.5];
    [layer setBorderColor:[[UIColor colorWithWhite: 0.8 alpha: 1.0] CGColor]];
  
  
  //Observer
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(RefreshPosterTable)
                                               name:@"Poster Added"
                                             object:nil];
  
}

- (void)viewDidUnload
{
  [super viewDidUnload];
  _CurrentObj = Nil;//empty local memo
  // Release any retained subviews of the main view.
  // e.g. self.myOutlet = nil;
    
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
  [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
  [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  // Return YES for supported orientations
  return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)didReceiveMemoryWarning
{
  // Releases the view if it doesn't have a superview.
  [super didReceiveMemoryWarning];
  
  // Release any cached data, images, etc that aren't in use.
}

#pragma mark - Parse

- (void)objectsDidLoad:(NSError *)error {
  [super objectsDidLoad:error];
  
  // This method is called every time objects are loaded from Parse via the PFQuery
}

- (void)objectsWillLoad {
  [super objectsWillLoad];
  
  // This method is called before a PFQuery is fired to get more objects
}


// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
  [_CurrentObj removeAllObjects];
  PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
  NSLog(@"QUERY FOR POSTER");
  
  query.cachePolicy = kPFCachePolicyIgnoreCache;
  [query whereKey:@"userName" equalTo:myusername];
  return query;
  
}



// Override to customize the look of a cell representing an object. The default is to display
// a UITableViewCellStyleDefault style cell with the label being the first key in the object.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
  static NSString *CellIdentifier = @"Cell";
  
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
  }
  
  // Configure the cell
  cell.textLabel.text = [NSString stringWithFormat:@"$%@",[object objectForKey:@"price"]]; //set text
  cell.textLabel.textColor = [UIColor whiteColor];
 
  NSNumber *issell = [object objectForKey:@"isSell"];
  NSLog(@"sell? %@",issell);
  
  if ([issell isEqualToNumber:[NSNumber numberWithInt:1]]) {
    [cell setBackgroundColor: [UIColor colorWithRed:135.0f/255.0f green:206.0f/255.0f blue:235.0f/255.0f alpha:1.0f]]; //set cell color
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ is selling", [object objectForKey:@"nickname"]];
  }
  else {
    [cell setBackgroundColor: [UIColor colorWithRed:152.0f/255.0f green:251.0f/255.0f blue:152.0f/255.0f alpha:1.0f]];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@ wants to buy", [object objectForKey:@"nickname"]];
  }

  
  [_CurrentObj addObject:object];
  NSLog(@"array size is: %i, OBjid is %@",_CurrentObj.count,[[_CurrentObj lastObject] objectId]);
  
  return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
  // Remove the row from data model
  PFObject *deleteOb = [_CurrentObj objectAtIndex:indexPath.row];
  [deleteOb delete];
  [_CurrentObj removeAllObjects];
  [self loadObjects];
  [tableView performSelector:@selector(reloadData) withObject:nil afterDelay:0.2];
  
}




#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
  [super tableView:tableView didSelectRowAtIndexPath:indexPath];
  NSLog(@"Select %lu",(long)indexPath.row);
  PFObject* obj = _CurrentObj[indexPath.row];
  NSLog(@"OBjid is %@",obj.objectId);
    [self EditPoster:obj.objectId];
  
}

-(void) EditPoster:(NSString*) objid {
    _posteraddingview = [[PosterAddingViewController alloc] init];
    _posteraddingview.isPT = YES;
    _posteraddingview.isEdit = YES;
    _posteraddingview.objid = objid;
    [_posteraddingview AlertShow];
}



-(void) RefreshPosterTable {
  [self loadObjects];
  NSLog(@"Poster Refreshed");
}


@end