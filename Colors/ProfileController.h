//
//  ProfileController.h
//  UMBooks_Beta
//
//  Created by Wen Yuchen on 4/9/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICSPlainColorViewController.h"
#import "ProfileTableViewController.h"
#import "SubclassConfigViewController.h"
#import "CourseViewController.h"
#import "BookViewController.h"
#import "PosterTableViewController.h"
#import "BookTableViewController.h"
#import "Parse/Parse.h"
@interface ProfileController : ICSPlainColorViewController
//sasa
@property ProfileTableViewController* myprofileviewcontroller;
@property(nonatomic, weak) ICSDrawerController *drawer;
@property NSNumber* myid;
@property UIButton* closeButton;

@property UIColor* clr;
@property int flag, ltoP;
@property UIView* topView;
@property UIButton *button1;
-(id)initWithLoginToProfile: (int)ltoP;
@end
