//
//  PosterTableViewController.h
//  UMBooks
//
//  Created by Shaoqing Zhu on 3/18/14.
//  Copyright (c) 2014 Shaoqing Zhu. All rights reserved.
//

#import <Parse/Parse.h>
#import "Datatypes.h"
#import "PosterAddingViewController.h"
#import "BookAddingViewController.h"
#import "CourseAddingViewController.h"
#import "FilterViewController.h"
@interface ProfileTableViewController : PFQueryTableViewController

@property  NSMutableArray* CurrentObj;
@property PosterAddingViewController *posteraddingview;
@property NSString* myusername;
@property NSNumber* myid;

-(id) initWithUsername:(NSString *)username;

@end


