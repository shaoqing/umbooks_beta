//
//  FilterViewController.m
//  UMBooks_Beta
//
//  Created by Shaoqing Zhu on 4/3/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "FilterViewController.h"

@interface FilterViewController ()

@end

@implementation FilterViewController
@synthesize myfilter,bookid;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
}
- (id)initWithBookID:(NSString *)bookid {
    
    myfilter = [[Filter alloc]init];
    bookid = bookid;
    
    return self;
}
-(void) AlertShow {
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self filterAddView]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"OK", @"Cancel", nil]];
    [alertView setDelegate:self];
    
    NSLog(@"Alert View Ready");
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
        if (buttonIndex == 0) {
            UIView* content = alertView.containerView;
            UISegmentedControl* sortpricecontrol = [content viewWithTag:1];
            UISegmentedControl* typecontrol = [content viewWithTag:3];
            UISegmentedControl* sorttimecontrol = [content viewWithTag:4];
            UISlider* priceRange = [content viewWithTag:2];
            
            myfilter.priceRange = NSMakeRange(0, priceRange.value);
            if ([typecontrol selectedSegmentIndex] == 0) {
                myfilter.isSell = [NSNumber numberWithInt:-1];
            }
            else if ([typecontrol selectedSegmentIndex] == 1) {
                myfilter.isSell = [NSNumber numberWithInt:1];
            }
            
            if ([sortpricecontrol selectedSegmentIndex] == 0) {
                myfilter.isDescPrice = [NSNumber numberWithInt:1];
            }
            else if ([sortpricecontrol selectedSegmentIndex] == 1) {
                myfilter.isDescPrice = [NSNumber numberWithInt:-1];
            }
            
            if ([sorttimecontrol selectedSegmentIndex] == 0) {
                myfilter.isDescCreatedTime = [NSNumber numberWithInt:1];
            }
            else if ([sorttimecontrol selectedSegmentIndex] == 1) {
                myfilter.isDescCreatedTime = [NSNumber numberWithInt:-1];
            }
            [[NSNotificationCenter defaultCenter] postNotification:[NSNotification                                                                notificationWithName:@"Filter"                                                                object:myfilter]];
        }
        
        [alertView close];
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
    [alertView close];
}


- (UIView *)filterAddView
{
    UIView *filterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 220, 350)];
    UILabel* pricesortlabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 200, 30)];
    pricesortlabel.text = @"Sorting Price By: ";
    UILabel* pricelabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 90, 200, 30)];
    pricelabel.text = @"Price Range: ";
    UILabel* typelabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 170, 200, 30)];
    typelabel.text = @"Sell or Buy?";
    
    
    UILabel* timesortlabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 250, 200, 30)];
    timesortlabel.text = @"Sorting Posting Time By: ";
    
    UISegmentedControl* sortprice = [[UISegmentedControl alloc]initWithFrame:CGRectMake(10, 50, 200, 30)];
    [sortprice insertSegmentWithTitle:@"Desc" atIndex:0 animated:YES];
    [sortprice insertSegmentWithTitle:@"Ascd" atIndex:1 animated:YES];
    sortprice.tag = 1;
    
    UISlider* pricerange = [[UISlider alloc] initWithFrame:CGRectMake(10, 130,200, 30)];
    pricerange.maximumValue = 200;
    pricerange.minimumValue = 0;
    pricerange.tag = 2;
    
    UISegmentedControl* type = [[UISegmentedControl alloc]initWithFrame:CGRectMake(10, 210, 200, 30)];
    [type insertSegmentWithTitle:@"BUY" atIndex:0 animated:YES];
    [type insertSegmentWithTitle:@"SELL" atIndex:1 animated:YES];
     type.tag = 3;
    
    UISegmentedControl* sorttime = [[UISegmentedControl alloc]initWithFrame:CGRectMake(10, 290, 200, 30)];
    [sorttime insertSegmentWithTitle:@"Desc" atIndex:0 animated:YES];
    [sorttime insertSegmentWithTitle:@"Ascd" atIndex:1 animated:YES];
    sorttime.tag = 4;
    
    
    [filterView addSubview:pricesortlabel];
    [filterView addSubview:sortprice];
    [filterView addSubview:pricelabel];
    [filterView addSubview:pricerange];
    [filterView addSubview:type];
    [filterView addSubview:typelabel];
    [filterView addSubview:timesortlabel];
    [filterView addSubview:sorttime];
    return filterView;
}
-(BOOL) textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}
@end
