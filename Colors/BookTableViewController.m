//
//  BookTableViewController.m
//  Colors
//
//  Created by Guan Wang on 3/19/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "BookTableViewController.h"
#import "ICSPlainColorViewController.h"
#import "CourseViewController.h"
#import "BookViewController.h"

static NSString * const kICSColorsViewControllerCellReuseId = @"kICSColorsViewControllerCellReuseId";



@interface BookTableViewController ()

@property(nonatomic, strong) NSArray *colors;
@property(nonatomic, assign) NSInteger previousRow;

@end



@implementation BookTableViewController
@synthesize myid;

- (id)initWithColors:(NSArray *)colors CourseID:(NSNumber *)courseid
{
    NSParameterAssert(colors);
    
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        _colors = colors;
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"Books";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"Title";
        
        // The title for this table in the Navigation Controller.
        self.title = @"Book";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 8;
        
    }
    _CurrentObj = [[NSMutableArray alloc] init];
     myid = courseid;
    NSLog(@"%@,",myid);
    _isMylist = FALSE;
    return self;
}

- (id)initWithColors:(NSArray *)colors
{
    NSParameterAssert(colors);
    
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        _colors = colors;
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"Books";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"Title";
        
        // The title for this table in the Navigation Controller.
        self.title = @"Book";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 8;
        
    }
    _CurrentObj = [[NSMutableArray alloc] init];
    _isMylist = YES;
    return self;
}
 
#pragma mark - Managing the view

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kICSColorsViewControllerCellReuseId];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    _CurrentObj = Nil;//empty local memo
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - Configuring the view’s layout behavior

- (UIStatusBarStyle)preferredStatusBarStyle
{
    // Even if this view controller hides the status bar, implementing this method is still needed to match the center view controller's
    // status bar style to avoid a flicker when the drawer is dragged and then left to open.
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell
    cell.textLabel.text = [object objectForKey:@"Title"];//set text
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [object objectForKey:@"Author"]]; //set small text
    [_CurrentObj addObject:object];
    //NSLog(@"array size is: %i, OBjid is %@",_CurrentObj.count,[[_CurrentObj lastObject] objectId]);
    cell.textLabel.textColor = [UIColor whiteColor];
    
    cell.backgroundColor = self.colors[indexPath.row%self.colors.count];
    return cell;
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    //parse
    NSLog(@"Select %lu",(long)indexPath.row);
    PFObject* obj = _CurrentObj[indexPath.row];
    NSLog(@"OBjid is %@",obj.objectId);
    
    NSNumber* bookid = [obj objectForKey:@"bookID"];
    NSLog(@"bookidstr is %@",bookid);
    
    NSString* bookdecrp = [NSString stringWithFormat: @"%@", [obj objectForKey:@"bookInfo"]];
    NSString* bookdauth = [NSString stringWithFormat:@"%@", [obj objectForKey:@"Author"]];
    NSString* bookdtitle = [NSString stringWithFormat:@"%@", [obj objectForKey:@"Title"]];
    NSLog(@"Course Description: %@", bookdecrp);//cout book description
    
    
    
    
    if (NO) {
        // Close the drawer without no further actions on the center view controller
        [self.drawer closer];
    }
    else {
        // Reload the current center view controller and update its background color
        /*
         typeof(self) __weak weakSelf = self;
         [self.drawer reloadCenterViewControllerUsingBlock:^(){
         NSParameterAssert(weakSelf.colors);
         weakSelf.drawer.centerViewController.view.backgroundColor = weakSelf.colors[indexPath.row%self.colors.count];
         }];
         */
        
        //        // Replace the current center view controller with a new one
        
        CourseViewController *center = [[CourseViewController alloc] initWithCourseName:bookdtitle Title:bookdauth Description:bookdecrp Flag:1];
        center.myid = bookid;//set book id //check later

        PosterTableViewController *back = [[PosterTableViewController alloc] initWithBookid:bookid withFilter:NULL];
        back.view.frame = CGRectMake(0, 50, 320, 590);
        BookViewController *book = [[BookViewController alloc] init];
        [book setFrontView:center];
        [book setBackView:back];
        center.view.backgroundColor = self.colors[indexPath.row%self.colors.count];
        [self.drawer replaceCenterViewControllerWithViewControllerr:book];
    }
    self.previousRow = indexPath.row;
    
    
    
}


#pragma mark - Parse

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    
    // This method is called every time objects are loaded from Parse via the PFQuery
}

- (void)objectsWillLoad {
    [super objectsWillLoad];
    
    // This method is called before a PFQuery is fired to get more objects
}


// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    //clear local data
    [_CurrentObj removeAllObjects];
    if (!_isMylist) {
        PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
        [query whereKey:@"courseID" equalTo:myid];
        [query orderByAscending:@"createdAt"];
        return query;
    }
    else {
        NSLog(@"mywatchlist");
        PFQuery *innerquery = [PFQuery queryWithClassName:@"UserBooks"];
        PFUser* myuser = [PFUser currentUser];
        [innerquery whereKey:@"Username" equalTo:myuser.username];
        
        PFQuery* result = [PFQuery queryWithClassName:self.parseClassName];
        [result whereKey:@"bookID" matchesKey:@"bookID" inQuery:innerquery];
        return result;
    }
}


#pragma mark - ICSDrawerControllerPresenting

- (void)drawerControllerWillOpen:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidOpen:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

- (void)drawerControllerWillClose:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidClose:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

@end
