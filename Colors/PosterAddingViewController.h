//
//  PosterAddingViewController.h
//  Colors
//
//  Created by Shaoqing Zhu on 3/27/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomIOS7AlertView.h"
#import "Datatypes.h"
#import "Parse/Parse.h"
#import "GCPlaceholderTextView.h"


@interface PosterAddingViewController : UIViewController<CustomIOS7AlertViewDelegate,UITextViewDelegate,UITextFieldDelegate>

@property Poster* poster;
@property BOOL isPT;
@property UITextField* namelabel ;
@property UITextField* price;
@property GCPlaceholderTextView* notecontent;
@property NSString* objid;
@property BOOL isEdit;


- (id)initWithBookID:(NSNumber *)bookid;
- (id) init;
-(void) AlertShow;
@end

