//
//  ICSColorsViewController.m
//
//  Created by Vito Modena
//
//  Copyright (c) 2014 ice cream studios s.r.l. - http://icecreamstudios.com
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of
//  this software and associated documentation files (the "Software"), to deal in
//  the Software without restriction, including without limitation the rights to
//  use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//  the Software, and to permit persons to whom the Software is furnished to do so,
//  subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
//  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#import "ICSColorsViewController.h"
#import "ICSPlainColorViewController.h"
#import "CourseViewController.h"
#import "BookTableViewController.h"


static NSString * const kICSColorsViewControllerCellReuseId = @"kICSColorsViewControllerCellReuseId";



@interface ICSColorsViewController ()

@property(nonatomic, strong) NSArray *colors;
@property(nonatomic, assign) NSInteger previousRow;

@end



@implementation ICSColorsViewController
@synthesize searchBar,searchDc,myid;

- (id)initWithColors:(NSArray *)colors ClassName:(NSString *)className
{
    NSParameterAssert(colors);
    
    self = [super initWithStyle:UITableViewStyleGrouped];
    if (self) {
        _colors = colors;
        // Custom the table
        
        // The className to query on
        self.parseClassName = @"Courses";
        
        // The key of the PFObject to display in the label of the default cell style
        self.textKey = @"courseTitle";
        
        // The title for this table in the Navigation Controller.
        self.title = @"Course";
        
        // Whether the built-in pull-to-refresh is enabled
        self.pullToRefreshEnabled = YES;
        
        // Whether the built-in pagination is enabled
        self.paginationEnabled = YES;
        
        // The number of objects to show per page
        self.objectsPerPage = 10;

    }
    _CurrentObj = [[NSMutableArray alloc] init];
    _titletobesearch = className;//initial with some value
    NSLog(@"load,%@",_titletobesearch);

    return self;
}

#pragma mark - Managing the view

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:kICSColorsViewControllerCellReuseId];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.searchBar=[[UISearchBar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 100.0f, 44.0f)];
    self.searchBar.tintColor=[UIColor colorWithRed:0.8f green:0.8f blue:0.8f alpha:1.0f];
    self.searchBar.autocorrectionType=UITextAutocorrectionTypeNo;
    self.searchBar.autocapitalizationType=UITextAutocapitalizationTypeNone;
    self.searchBar.keyboardType=UIKeyboardTypeAlphabet;
    self.searchBar.hidden=NO;
    self.searchBar.delegate = self;
    self.searchBar.placeholder=[NSString stringWithCString:"Enter the Course Number"  encoding: NSUTF8StringEncoding];
    self.tableView.tableHeaderView=self.searchBar;
    NSLog(@"aaaaaaa%f", self.tableView.tableHeaderView.frame.size.width);
    self.searchDc=[[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self] ;
    self.searchDc.searchResultsDataSource=self;
    self.searchDc.searchResultsDelegate=self;
    self.searchDc.delegate = self;
    [self.searchDc  setActive:YES];

    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [_CurrentObj removeAllObjects];//empty local memo
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
    //[self startTimer];
}


-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    NSLog(@"aaaaaaaaaaa%@",searchString);
    _titletobesearch = searchString;
    [self loadObjects];
    
    
    
    double delayInSeconds = 500;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_MSEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        NSLog(@"Delayed");
        [self.searchDc.searchResultsTableView reloadData];
        [_CurrentObj removeAllObjects];
        NSLog(@"RELOADED");
        
    });
    NSLog(@"return after cell");
    return YES;
}

-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self.searchDc.searchResultsTableView reloadData];
    [_CurrentObj removeAllObjects];
    NSLog(@"Search Button Clicked");
}

#pragma mark - Configuring the view’s layout behavior

- (UIStatusBarStyle)preferredStatusBarStyle
{
    // Even if this view controller hides the status bar, implementing this method is still needed to match the center view controller's
    // status bar style to avoid a flicker when the drawer is dragged and then left to open.
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark - Table view data source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath object:(PFObject *)object {
    
    static NSString *CellIdentifier = @"Cell";
    //NSLog(@"Entering Cell Configure");
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    // Configure the cell
    
    cell.textLabel.text = [object objectForKey:@"courseTitle"]; //set text
    
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [object objectForKey:@"courseName"]]; //set small text
    [_CurrentObj addObject:object];
    NSLog(@"array size is: %i, OBjid is %@",_CurrentObj.count,[[_CurrentObj lastObject] objectId]);
    
    cell.textLabel.textColor = [UIColor whiteColor];
    
    cell.backgroundColor = self.colors[indexPath.row%self.colors.count];
    NSLog(@"Index at: %ld, CELL CONTENT: %@,",(long)indexPath.row,cell.textLabel.text);
    return cell;
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    //parse
    NSLog(@"Select %lu",(long)indexPath.row);
    PFObject* obj = _CurrentObj[indexPath.row];
    // NSLog(@"OBjid is %@",obj.objectId);
    
    NSString* coursedecrp = [NSString stringWithFormat: @"%@", [obj objectForKey:@"courseDecrp"]];
    NSString* coursetitle = [NSString stringWithFormat: @"%@", [obj objectForKey:@"courseTitle"]];
    NSString* coursename = [NSString stringWithFormat:@"%@", [obj objectForKey:@"courseName"]];
    myid = [obj objectForKey:@"courseID"];
    // NSLog(@"Course Description: %@", coursedecrp);//cout course description

    
    
    
    if (NO) {
        // Close the drawer without no further actions on the center view controller
        [self.drawer close];
    }
    else {
        // Reload the current center view controller and update its background color
        /* 
        typeof(self) __weak weakSelf = self;
        [self.drawer reloadCenterViewControllerUsingBlock:^(){
            NSParameterAssert(weakSelf.colors);
            weakSelf.drawer.centerViewController.view.backgroundColor = weakSelf.colors[indexPath.row%self.colors.count];
        }];
         */
        
        //        // Replace the current center view controller with a new one
        
        CourseViewController *center = [[CourseViewController alloc] initWithCourseName:coursename Title:coursetitle Description:coursedecrp];
        
        center.myid = myid;
        
        center.view.backgroundColor = self.colors[indexPath.row%self.colors.count];
        [self.drawer replaceCenterViewControllerWithViewController:center];
        
        NSArray *colorsr = @[[UIColor colorWithRed:0.0f/255.0f green:250.0f/255.0f blue:154.0f/255.0f alpha:1.0f],
                            [UIColor colorWithRed:100.0f/255.0f green:149.0f/255.0f blue:237.0f/255.0f alpha:1.0f],
                            [UIColor colorWithRed:255.0f/255.0f green:105.0f/255.0f blue:180.0f/255.0f alpha:1.0f]
                            ];
        BookTableViewController *right = [[BookTableViewController alloc] initWithColors:colorsr CourseID:myid];
        //BookTableViewController *right = [[BookTableViewController alloc] initWithColors:colorsr];
        [self.drawer replaceRightViewControllerWithViewController:right];
    }
    self.previousRow = indexPath.row;
    
    
    
}
#pragma mark - Parse

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    NSLog(@"OBJ Did Load, %@",_titletobesearch);
    [_CurrentObj removeAllObjects];
    // This method is called every time objects are loaded from Parse via the PFQuery
}

- (void)objectsWillLoad {
    [super objectsWillLoad];
    // This method is called before a PFQuery is fired to get more objects
}


// Override to customize what kind of query to perform on the class. The default is to query for
// all objects ordered by createdAt descending.
- (PFQuery *)queryForTable {
    [_CurrentObj removeAllObjects];
    //PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    NSLog(@"query %@",_titletobesearch);
    PFQuery *query = [self SearchCourseByTitle:_titletobesearch];
    
    //PFObject* fobj = [query getFirstObject];
    //NSLog(@"First Obj: %@,",[fobj objectForKey:@"courseTitle"]);
    
    return query;
}
//UMBooks: Search for books
-(PFQuery*) SearchCourseByTitle: (NSString*) title {
    PFQuery *query = [PFQuery queryWithClassName:@"Courses"];
    [query whereKey:@"courseTitle" hasPrefix:title];
    return query;
}


#pragma mark - ICSDrawerControllerPresenting

- (void)drawerControllerWillOpen:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidOpen:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

- (void)drawerControllerWillClose:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = NO;
}

- (void)drawerControllerDidClose:(ICSDrawerController *)drawerController
{
    self.view.userInteractionEnabled = YES;
}

@end
