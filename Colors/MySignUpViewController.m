//
//  MySignUpViewController.m
//  LogInAndSignUpDemo
//
//  Created by Yuchen Wen on 4/1/14.
//  Copyright 2014 Yuchen Wen All rights reserved.
//

#import "MySignUpViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Datatypes.h"

@interface MySignUpViewController ()
@property (nonatomic, strong) UIImageView *fieldsBackground;
@end

@implementation MySignUpViewController

@synthesize fieldsBackground;

- (void)viewDidLoad {
    [super viewDidLoad];
  
  [self.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"UMBookslogo3.png"]]];
  NSArray *colors = @[[UIColor colorWithRed:237.0f/255.0f green:195.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:147.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:9.0f/255.0f blue:0.0f/255.0f alpha:1.0f]
                      ];
  
  [self.signUpView setBackgroundColor:colors[0]];
  fieldsBackground = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signback.png"]];
  [self.fieldsBackground setFrame:CGRectMake(35.0f, 132.0f, 250.0f, 200.0f)];
    if ([Constant longscreen]) {
       [self.fieldsBackground setFrame:CGRectMake(35.0f, 176.0f, 250.0f, 200.0f)];
    
    }
  [self.signUpView addSubview:self.fieldsBackground];
  [self.signUpView sendSubviewToBack:self.fieldsBackground];
  [self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@"signup.png"] forState:UIControlStateNormal];
  [self.signUpView.signUpButton setBackgroundImage:[UIImage imageNamed:@"signup.png"] forState:UIControlStateHighlighted];
    
  
    // Remove text shadow
    CALayer *layer = self.signUpView.usernameField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.passwordField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.emailField.layer;
    layer.shadowOpacity = 0.0f;
    layer = self.signUpView.additionalField.layer;
    layer.shadowOpacity = 0.0f;
    // Set text color
    [self.signUpView.usernameField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
    [self.signUpView.passwordField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
    [self.signUpView.emailField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
  [self.signUpView.additionalField setTextColor:[UIColor colorWithRed:135.0f/255.0f green:118.0f/255.0f blue:92.0f/255.0f alpha:1.0]];
  self.signUpView.usernameField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"User Name" attributes:@{NSForegroundColorAttributeName: colors[1]}];
  self.signUpView.passwordField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: colors[1]}];
    self.signUpView.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: colors[1]}];
    // Change "Additional" to match our use
      self.signUpView.additionalField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Nick Name" attributes:@{NSForegroundColorAttributeName: colors[1]}];
    
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
           [self.signUpView.logo setFrame:CGRectMake(120.0f, 30.0f, 80.0f, 80.0f)];
  [self.signUpView.signUpButton setTitle:@"" forState:UIControlStateNormal];
  [self.signUpView.signUpButton setTitle:@"" forState:UIControlStateHighlighted];
    [self.signUpView.signUpButton setFrame:CGRectMake(35.0f, 360.0f, 250.0f, 50.0f)];
    if ([Constant longscreen]) {
        [self.signUpView.signUpButton setFrame:CGRectMake(35.0f, 436.0f, 250.0f, 40.0f)];
        
    }

    // Move all fields down on smaller screen sizes
  /*  float yOffset = [UIScreen mainScreen].bounds.size.height <= 480.0f ? 30.0f : 0.0f;

    CGRect fieldFrame = self.signUpView.usernameField.frame;

    [self.signUpView.dismissButton setFrame:CGRectMake(10.0f, 10.0f, 87.5f, 45.5f)];
    [self.signUpView.logo setFrame:CGRectMake(66.5f, 70.0f, 187.0f, 58.5f)];
    [self.signUpView.signUpButton setFrame:CGRectMake(35.0f, 385.0f, 250.0f, 40.0f)];
    [self.fieldsBackground setFrame:CGRectMake(35.0f, fieldFrame.origin.y + yOffset, 250.0f, 174.0f)];
    
    [self.signUpView.usernameField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                       fieldFrame.origin.y + yOffset,
                                                       fieldFrame.size.width - 10.0f,
                                                       fieldFrame.size.height)];
    yOffset += fieldFrame.size.height;
    
    [self.signUpView.passwordField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                       fieldFrame.origin.y + yOffset,
                                                       fieldFrame.size.width - 10.0f,
                                                       fieldFrame.size.height)];
    yOffset += fieldFrame.size.height;
    
    [self.signUpView.emailField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                    fieldFrame.origin.y + yOffset,
                                                    fieldFrame.size.width - 10.0f,
                                                    fieldFrame.size.height)];
    yOffset += fieldFrame.size.height;
    
    [self.signUpView.additionalField setFrame:CGRectMake(fieldFrame.origin.x + 5.0f,
                                                         fieldFrame.origin.y + yOffset,
                                                         fieldFrame.size.width - 10.0f,
                                                         fieldFrame.size.height)];
   */
  

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
