//
//  Datatypes.h
//  UMBooks
//
//  Created by Shaoqing Zhu on 3/17/14.
//  Copyright (c) 2014 Shaoqing Zhu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constant : NSObject
-(id)initWithBool:(BOOL)isiphone5;
+(BOOL) longscreen;
@end

@interface Book : NSObject
    @property NSString *isbn;//ISBN auth
    @property NSNumber *courseid;
    @property NSString *title;
    @property NSString *author;
    @property NSString *bookinfo;
-(id)init;
@end

@interface Course : NSObject
    @property NSString *courseTitle;//courseid
    @property NSString *courseName;
    @property NSString *courseDescrp;
-(id)init;
@end

@interface Poster : NSObject
    @property NSString* posterid;
    @property NSString *username;
    @property NSNumber *bookid;
    @property NSString* nickname;
    @property NSNumber *isSell;
    @property NSNumber *price;
    @property NSString *note;//
    @property NSString* email;
-(id)init;
@end


@interface Filter : NSObject
@property NSNumber* sortOn;
@property NSNumber* isDesc;
@property NSRange priceRange;
@property NSNumber* isSell;
-(id)init;
@end