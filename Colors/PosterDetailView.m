//
//  PosterDetailView.m
//  UMBooks_Beta
//
//  Created by Shaoqing Zhu on 4/11/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "PosterDetailView.h"

@interface PosterDetailView ()

@end

@implementation PosterDetailView
@synthesize email,note;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void) AlertShow {
    
    CustomIOS7AlertView *alertView = [[CustomIOS7AlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self PosterView]];
    
    // Modify the parameters
    [alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Contact", @"Cancel", nil]];
    [alertView setDelegate:self];
    
    NSLog(@"Alert View Ready");
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
        if (buttonIndex == 0) {
            NSLog(@"Contact Me,%@",email);
            [alertView close];
            [[NSNotificationCenter defaultCenter] postNotification:[NSNotification                                                                notificationWithName:@"Email"                                                                object:email]];
        }
        else if (buttonIndex == 1) {
            [alertView close];
        }
        
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];
}
- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
    [alertView close];
}


- (UIView *)PosterView
{
    UIView *posterview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 200)];
    UILabel* emaillabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 280, 60)];
    emaillabel.numberOfLines= 0;
    NSLog(@"email: %@, note: %@",email,note);
    emaillabel.text = [NSString stringWithFormat:@"%@", email];
    UILabel* notelabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 50, 280, 60)];
    notelabel.text = [NSString stringWithFormat:@"Note: %@", note];
    notelabel.numberOfLines= 0;
    NSLog(@"email: %@, note: %@",emaillabel.text,notelabel.text);
    UILabel* infolabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 120, 280, 100)];
    infolabel.text = @"If interested in this ad, please contact me via email";
    infolabel.textColor = [UIColor grayColor];
    infolabel.numberOfLines= 0;
    [posterview addSubview:emaillabel];
    [posterview addSubview:notelabel];
    [posterview addSubview:infolabel];
    return posterview;
}


@end
