//
//  CourseViewController.h
//  Colors
//
//  Created by Guan Wang on 3/19/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "ICSPlainColorViewController.h"
#import "CourseViewController.h"
#import "PosterAddingViewController.h"
#import "BookAddingViewController.h"
#import "CourseAddingViewController.h"
#import "FilterViewController.h"
#import "BookTableViewController.h"

@interface CourseViewController : ICSPlainColorViewController

@property UITextView *titleView;
@property UITextView *nameView;
@property UITextView *descrView;
@property PosterAddingViewController *posteraddingview;
@property BookAddingViewController* bookaddingview;
@property CourseAddingViewController* courseaddingview;
@property FilterViewController* filterview;
@property NSNumber* myid;
@property NSString* username;
@property NSString* nickname;
@property int flag;
@property UIView* topView;
@property (strong, nonatomic) UIButton * aButton;
@property (strong, nonatomic) UIButton * daButton;


- (id)initWithCourseName:(NSString *)name Title:(NSString *)title Description:(NSString *)description;
- (id)initWithCourseName:(NSString *)name Title:(NSString *)title Description:(NSString *)description Flag:(int)f;
- (void)showMenu;

@end
