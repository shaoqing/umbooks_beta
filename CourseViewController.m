//
//  CourseViewController.m
//  Colors
//
//  Created by Guan Wang on 3/19/14.
//  Copyright (c) 2014 ice cream studios s.r.l. All rights reserved.
//

#import "CourseViewController.h"
#import "CHTumblrMenuView.h"
#import "GHContextMenuView.h"
#import "ProfileController.h"

@interface CourseViewController ()<UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, GHContextOverlayViewDataSource, GHContextOverlayViewDelegate>

@property(nonatomic, strong) UIButton *openDrawerButtonr;

@end

@implementation CourseViewController
@synthesize titleView,descrView,nameView,myid,nickname,username,flag,aButton,daButton,topView;


- (id)initWithCourseName:(NSString *)name Title:(NSString *)title Description:(NSString *)description {
    self = [super init];
    self.flag = 0;
    if (self) {
        // Custom initialization
        // NSLog(@"Course Description: %@", description);
        self.titleView=[[UITextView alloc] initWithFrame:CGRectMake(9.0f, 60.0f, 310.0f, 45.0f)];
        self.titleView.font = [UIFont fontWithName:nil size:36.0];
        self.titleView.textColor = [UIColor whiteColor];
        self.titleView.backgroundColor = [UIColor clearColor];
        self.titleView.text = title;
        self.titleView.editable = NO;
        [self.view addSubview:titleView];
        self.nameView=[[UITextView alloc] initWithFrame:CGRectMake(10.0f, 100.0f, 310.0f, 50.0f)];
        self.nameView.font = [UIFont fontWithName:nil size:18.0];
        self.nameView.textColor = [UIColor blackColor];
        self.nameView.backgroundColor = [UIColor clearColor];
        self.nameView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.nameView.text = name;
        self.nameView.editable = NO;
        [self.view addSubview:nameView];
        self.descrView=[[UITextView alloc] initWithFrame:CGRectMake(10.0f, 140.0f, 300.0f, 194.0f)];
        self.descrView.font = [UIFont fontWithName:nil size:16.0];
        self.descrView.textColor = [UIColor whiteColor];
        self.descrView.backgroundColor = [UIColor clearColor];
        self.descrView.text = description;
        self.descrView.editable = NO;
        self.descrView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:descrView];
        UIImage *hamburger = [UIImage imageNamed:@"hamburger"];
        NSParameterAssert(hamburger);
        
        self.openDrawerButtonr = [UIButton buttonWithType:UIButtonTypeCustom];
        self.openDrawerButtonr.frame = CGRectMake(266.0f, 20.0f, 44.0f, 44.0f);
        [self.openDrawerButtonr setImage:hamburger forState:UIControlStateNormal];
        [self.openDrawerButtonr addTarget:self action:@selector(openDrawerr:) forControlEvents:UIControlEventTouchUpInside];
        self.openDrawerButtonr.alpha = 0;
        [self.view addSubview:self.openDrawerButtonr];
        
      UIButton *profileButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
      [profileButton setTitle:@"To Profile" forState:UIControlStateNormal];
      profileButton.frame = CGRectMake(150, 25, 90, 30);
      [profileButton addTarget:self action:@selector(profileListener:) forControlEvents:UIControlEventTouchUpInside];
     // [self.view addSubview:profileButton];
        
    }
    return self;

    
}
- (id)initWithCourseName:(NSString *)name Title:(NSString *)title Description:(NSString *)description Flag:(int)f{
    self = [super init];
    self.flag = 1;
    if (self) {
        // Custom initialization
        // NSLog(@"Course Description: %@", description);
        self.titleView=[[UITextView alloc] initWithFrame:CGRectMake(9.0f, 60.0f, 310.0f, 45.0f)];
        self.titleView.font = [UIFont fontWithName:nil size:36.0];
        self.titleView.textColor = [UIColor whiteColor];
        self.titleView.backgroundColor = [UIColor clearColor];
        self.titleView.text = title;
        self.titleView.editable = NO;
        [self.view addSubview:titleView];
        self.nameView=[[UITextView alloc] initWithFrame:CGRectMake(10.0f, 100.0f, 310.0f, 50.0f)];
        self.nameView.font = [UIFont fontWithName:nil size:18.0];
        self.nameView.textColor = [UIColor blackColor];
        self.nameView.backgroundColor = [UIColor clearColor];
        self.nameView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.nameView.text = name;
        self.nameView.editable = NO;
        [self.view addSubview:nameView];
        self.descrView=[[UITextView alloc] initWithFrame:CGRectMake(10.0f, 140.0f, 300.0f, 194.0f)];
        self.descrView.font = [UIFont fontWithName:nil size:16.0];
        self.descrView.textColor = [UIColor whiteColor];
        self.descrView.backgroundColor = [UIColor clearColor];
        self.descrView.text = description;
        self.descrView.editable = NO;
        self.descrView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:descrView];
        UIImage *hamburger = [UIImage imageNamed:@"hamburger"];
        NSParameterAssert(hamburger);
        
        self.openDrawerButtonr = [UIButton buttonWithType:UIButtonTypeCustom];
        self.openDrawerButtonr.frame = CGRectMake(266.0f, 20.0f, 44.0f, 44.0f);
        [self.openDrawerButtonr setImage:hamburger forState:UIControlStateNormal];
        [self.openDrawerButtonr addTarget:self action:@selector(openDrawerr:) forControlEvents:UIControlEventTouchUpInside];
        self.openDrawerButtonr.alpha = 0;
        [self.view addSubview:self.openDrawerButtonr];
        
        UIButton *profileButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [profileButton setTitle:@"To Profile" forState:UIControlStateNormal];
        profileButton.frame = CGRectMake(150, 25, 90, 30);
        [profileButton addTarget:self action:@selector(profileListener:) forControlEvents:UIControlEventTouchUpInside];
        // [self.view addSubview:profileButton];
        
    }
    return self;
    
    

}
-(void)profileListener:(UIButton *) sender {
  NSArray *colors = @[[UIColor colorWithRed:237.0f/255.0f green:195.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:147.0f/255.0f blue:0.0f/255.0f alpha:1.0f],
                      [UIColor colorWithRed:237.0f/255.0f green:9.0f/255.0f blue:0.0f/255.0f alpha:1.0f]
                      ];
  ProfileController *profile = [[ProfileController alloc] init];
  profile.view.backgroundColor = [UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
    profile.myid = myid;
    profile.clr = self.view.backgroundColor;
    profile.flag = flag;
  [self.drawer replaceCenterViewControllerWithViewControllerr:profile];
    NSArray *colorsr = @[[UIColor colorWithRed:0.0f/255.0f green:250.0f/255.0f blue:154.0f/255.0f alpha:1.0f],
                         [UIColor colorWithRed:100.0f/255.0f green:149.0f/255.0f blue:237.0f/255.0f alpha:1.0f],
                         [UIColor colorWithRed:255.0f/255.0f green:105.0f/255.0f blue:180.0f/255.0f alpha:1.0f]
                         ];
    
    BookTableViewController *right = [[BookTableViewController alloc] initWithColors:colorsr];
    [self.drawer replaceRightViewControllerWithViewController:right];
}
#pragma mark - Open drawer button

- (void)openDrawerr:(id)sender
{
    [self.drawer openr];
    
}

- (void)showMenu0
{
    CHTumblrMenuView *menuView = [[CHTumblrMenuView alloc] init];
    [menuView addMenuItemWithTitle:@"Add Book" andIcon:[UIImage imageNamed:@"addbook.gif"] andSelectedBlock:^{
        [self AddBook];
        NSLog(@"Text selected");
    }];
    [menuView addMenuItemWithTitle:@"Edit Course" andIcon:[UIImage imageNamed:@"editcourse.gif"] andSelectedBlock:^{
        [self EditCourse];
        NSLog(@"Chat selected");
        
    }];
    
    [menuView addMenuItemWithTitle:@"Add Course" andIcon:[UIImage imageNamed:@"addcourse.gif"] andSelectedBlock:^{
        [self AddCourse];
        NSLog(@"Link selected");
        
    }];
    
    
    
    [menuView show];
}


- (void)showMenu1
{
    CHTumblrMenuView *menuView = [[CHTumblrMenuView alloc] init];
        [menuView addMenuItemWithTitle:@"Edit Book" andIcon:[UIImage imageNamed:@"editbook.gif"] andSelectedBlock:^{
            [self EditBook];
        NSLog(@"Photo selected");
    }];
    [menuView addMenuItemWithTitle:@"Add Poster" andIcon:[UIImage imageNamed:@"addposter.gif"] andSelectedBlock:^{
        [self AddPoster];
        NSLog(@"Quote selected");
        
    }];
    [menuView addMenuItemWithTitle:@"Add Course" andIcon:[UIImage imageNamed:@"addcourse.gif"] andSelectedBlock:^{
        [self AddCourse];
        NSLog(@"Link selected");
        
    }];
    [menuView addMenuItemWithTitle:@"Add to watch list" andIcon:[UIImage imageNamed:@"watchlist.gif"] andSelectedBlock:^{
        [self Feature];
        NSLog(@"Link selected");
        
    }];
    
    
    
    [menuView show];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [UIView animateWithDuration:2
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut animations:^(void){
                            self.openDrawerButtonr.alpha = 0.0;
                        }completion:^(BOOL finished){
                            [UIView animateWithDuration:1
                                                  delay:1.0
                                                options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionRepeat
                                             animations:^(void){
                                                 [UIView setAnimationRepeatCount:2.5];
                                                 self.openDrawerButtonr.alpha = 1.0;
                                             }completion:^(BOOL finished){
                                                 
                                             }];
                            
                        }];
    
    
    self.aButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.aButton.frame = CGRectMake(200, 360, 80, 80);
    if ([Constant longscreen]) {
        self.aButton.frame = CGRectMake(200, 448, 80, 80);
    }
    UIImage *btnImage = [UIImage imageNamed:@"menu.gif"];
    //UIImage *btnImage2 = [UIImage imageNamed:@"post_type_bubble_link@2x.png"];
    [self.aButton setBackgroundImage:btnImage forState:UIControlStateNormal];
    [self.aButton setBackgroundImage:btnImage forState:UIControlStateHighlighted];
    
    
    [self.aButton addTarget:self action:@selector(FlipButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.aButton];
    
    self.daButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.daButton.frame = CGRectMake(40, 360, 80, 80);
    if ([Constant longscreen]) {
        self.daButton.frame = CGRectMake(40, 448, 80, 80);
    }
    UIImage *btnImagez = [UIImage imageNamed:@"user.gif"];
    //UIImage *btnImage2 = [UIImage imageNamed:@"post_type_bubble_link@2x.png"];
    [self.daButton setBackgroundImage:btnImagez forState:UIControlStateNormal];
    [self.daButton setBackgroundImage:btnImagez forState:UIControlStateHighlighted];
    
    
    [self.daButton addTarget:self action:@selector(profileListener:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.daButton];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(CourseUpdate:)
                                                 name:@"Course Edited"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(BookUpdate:)
                                                 name:@"Book Edited"
                                               object:nil];
    NSLog(@"asaaaaaaaaaaaaa%i",self.flag);
    /*if (self.flag == 0) {
        [self showTutorialOverlay];
    }
    if (self.flag == 1) {
        [self showTutorialOverlay1];
    }*/
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"c"] && self.flag == 0) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"c"];
        [self showTutorialOverlay];
    }
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"b"] && self.flag == 1) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"b"];
        [self showTutorialOverlay1];
    }

}

- (void)FlipButtonAction:(id)sender {
    if (flag == 0){
        self.showMenu0;
    } else if (flag == 1){
        self.showMenu1;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - GHMenu methods

- (NSInteger) numberOfMenuItems
{
    return 5;
}

-(UIImage*) imageForItemAtIndex:(NSInteger)index
{
    NSString* imageName = nil;
    switch (index) {
        case 0:
            imageName = @"facebook-white";
            break;
        case 1:
            imageName = @"twitter-white";
            break;
        case 2:
            imageName = @"google-plus-white";
            break;
        case 3:
            imageName = @"linkedin-white";
            break;
        case 4:
            imageName = @"pinterest-white";
            break;
            
        default:
            break;
    }
    return [UIImage imageNamed:imageName];
}

- (void) didSelectItemAtIndex:(NSInteger)selectedIndex forMenuAtPoint:(CGPoint)point
{
    NSString* msg = nil;
    switch (selectedIndex) {
        case 0:
            msg = @"Facebook Selected";
            break;
        case 1:
            msg = @"Twitter Selected";
            break;
        case 2:
            msg = @"Google Plus Selected";
            break;
        case 3:
            msg = @"Linkedin Selected";
            break;
        case 4:
            msg = @"Pinterest Selected";
            break;
            
        default:
            break;
    }
    NSLog(@"%@",msg);
    NSLog(@"%@----------%@",[self class],[super class]);
    if (flag == 0){
        self.showMenu0;
    } else if (flag == 1){
        self.showMenu1;
    }
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    //[alertView show];
    
}

-(void) AddPoster {
    _posteraddingview = [[PosterAddingViewController alloc] initWithBookID:myid ];
    _posteraddingview.isPT = FALSE;
    _posteraddingview.isEdit = FALSE;
    [_posteraddingview AlertShow];
}
-(void) EditBook {
    _bookaddingview = [[BookAddingViewController alloc] initWithCourseID:myid];
    _bookaddingview.isEdit = YES;
    NSLog(@"Edit Book in 1 course: %@",myid);
    _bookaddingview.myid = myid;
    NSLog(@"Edit Book in 2 course: %@",myid);
    [_bookaddingview AlertShow];

    
}
-(void) AddBook {
    _bookaddingview = [[BookAddingViewController alloc] initWithCourseID:myid];
    _bookaddingview.isEdit =FALSE;
    [_bookaddingview AlertShow];
    NSArray *colorsr = @[[UIColor colorWithRed:0.0f/255.0f green:250.0f/255.0f blue:154.0f/255.0f alpha:1.0f],
                         [UIColor colorWithRed:100.0f/255.0f green:149.0f/255.0f blue:237.0f/255.0f alpha:1.0f],
                         [UIColor colorWithRed:255.0f/255.0f green:105.0f/255.0f blue:180.0f/255.0f alpha:1.0f]
                         ];
    BookTableViewController *right = [[BookTableViewController alloc] initWithColors:colorsr CourseID:myid];
    [self.drawer replaceRightViewControllerWithViewController:right];
        
}
-(void) EditCourse {
    _courseaddingview = [[CourseAddingViewController alloc]init];
    _courseaddingview.isEdit = YES;
    _courseaddingview.myid = myid;
    [_courseaddingview AlertShow];
}
-(void) AddCourse {
    _courseaddingview = [[CourseAddingViewController alloc]init];
    _courseaddingview.isEdit = FALSE;
    [_courseaddingview AlertShow];
}

-(void) AddFilter {
    _filterview = [[FilterViewController alloc] initWithBookID:myid];
    [_filterview AlertShow];
}

-(void) Feature {
    PFUser* myuser = [PFUser currentUser];
    
    PFQuery* query = [[PFQuery alloc]initWithClassName:@"UserBooks"];
    [query whereKey:@"Username" equalTo:myuser.username];
    [query whereKey:@"bookID" equalTo:myid];
    [query countObjectsInBackgroundWithBlock:^(int number, NSError* error) {
        if (number == 0) {
            NSLog(@"new book featured");
            PFObject* mybook = [PFObject objectWithClassName:@"UserBooks"];
            NSLog(@"%@ featured book %@",myuser.username,myid);
            [mybook setObject:myuser.username forKey:@"Username"];
            [mybook setObject:myid forKey:@"bookID"];
            [mybook saveInBackground];
        }
        else {
            NSLog(@"Already featured this one");
        }
        
    }];
}

-(void) CourseUpdate:(NSNotification *)notification {
    Course *cs = [notification object];
    self.titleView.text = cs.courseTitle;
    self.nameView.text = cs.courseName;
    self.descrView.text = cs.courseDescrp;
    
    NSLog(@"Course Page Updated");
}

-(void) BookUpdate:(NSNotification *)notification {
    Book *bk = [notification object];
    self.titleView.text = bk.author;
    self.nameView.text = bk.title;
    self.descrView.text = bk.bookinfo;
    NSArray *colorsr = @[[UIColor colorWithRed:0.0f/255.0f green:250.0f/255.0f blue:154.0f/255.0f alpha:1.0f],
                         [UIColor colorWithRed:100.0f/255.0f green:149.0f/255.0f blue:237.0f/255.0f alpha:1.0f],
                         [UIColor colorWithRed:255.0f/255.0f green:105.0f/255.0f blue:180.0f/255.0f alpha:1.0f]
                         ];
    
    BookTableViewController *right = [[BookTableViewController alloc] initWithColors:colorsr CourseID:myid];
    [self.drawer replaceRightViewControllerWithViewController:right];
    
    NSLog(@"Book Page Updated");
}


-(void) showTutorialOverlay
{
    if ([Constant longscreen]) {
        topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
        
        UIView *tutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
        [tutView setBackgroundColor:[UIColor blackColor]];
        [tutView setAlpha:0.3];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = topView.frame;
        [button addTarget:self action:@selector(hideTutorialOverlay) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
        
        UIImageView *tutImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"courser.gif"]];
        
        [tutImageView setFrame:CGRectMake(0, (-1) * statusBarFrame.size.height, 320, 568)];
        [tutView addSubview:button];
        
        [topView addSubview:tutView];
        [topView addSubview:tutImageView];
        [[UIApplication sharedApplication].keyWindow addSubview:topView];
        
    }
    else {
        topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
        
        UIView *tutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
        [tutView setBackgroundColor:[UIColor blackColor]];
        [tutView setAlpha:0.3];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = topView.frame;
        [button addTarget:self action:@selector(hideTutorialOverlay) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
        
        UIImageView *tutImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ocourse.gif"]];
        
        [tutImageView setFrame:CGRectMake(0, (-1) * statusBarFrame.size.height, 320, 480)];
        [tutView addSubview:button];

        [topView addSubview:tutView];
        [topView addSubview:tutImageView];
        [[UIApplication sharedApplication].keyWindow addSubview:topView];
    }
    
    
    
}

-(void) showTutorialOverlay1

{
    if ([Constant longscreen]) {
        topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
        
        UIView *tutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
        [tutView setBackgroundColor:[UIColor blackColor]];
        [tutView setAlpha:0.3];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = topView.frame;
        [button addTarget:self action:@selector(hideTutorialOverlay1) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
        
        UIImageView *tutImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bookr.gif"]];
        [tutImageView setFrame:CGRectMake(0, (-1) * statusBarFrame.size.height, 320, 568)];
        
        
        [tutView addSubview:button];
        [topView addSubview:tutView];
        [topView addSubview:tutImageView];
        [[UIApplication sharedApplication].keyWindow addSubview:topView];
        
    }
    else {
        topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
        
        UIView *tutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 480)];
        [tutView setBackgroundColor:[UIColor blackColor]];
        [tutView setAlpha:0.3];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = topView.frame;
        [button addTarget:self action:@selector(hideTutorialOverlay1) forControlEvents:UIControlEventTouchUpInside];
        
        CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
        
        UIImageView *tutImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"obook.gif"]];
        [tutImageView setFrame:CGRectMake(0, (-1) * statusBarFrame.size.height, 320, 480)];
        
        
        [tutView addSubview:button];
        [topView addSubview:tutView];
        [topView addSubview:tutImageView];
        [[UIApplication sharedApplication].keyWindow addSubview:topView];

    }
    
}

-(void)hideTutorialOverlay{
    [topView removeFromSuperview];
}
-(void)hideTutorialOverlay1{
    [topView removeFromSuperview];
}
@end
